# frozen_string_literal: true

require 'item_builder/version'
require 'item_builder/modes'
require 'item_builder/modes/base_service'
require 'item_builder/modes/price_service'
require 'item_builder/modes/quantity_service'
require 'item_builder/modes/simple_service'
require 'item_builder/modes/active_service'
require 'item_builder/modes/create_service'
require 'item_builder/modes/map_create_service'
require 'item_builder/modes/precondition_create_service'
require 'item_builder/modes/update_service'
require 'item_models'
require 'item_builder/zilingo_quantity_service'
require 'item_builder/lazada_quantity_service'
require 'item_builder/zalora_quantity_service'
require 'item_builder/shopify_quantity_service'
class ItemBuilder
  def self.build(listing_ids, mode)
    new(listing_ids, mode).mode_check
  end

  attr_reader :listing_ids
  attr_reader :listings
  attr_reader :mode
  attr_reader :wh_spaces
  attr_reader :variants
  attr_reader :variant_ids
  attr_reader :wh_mapping
  attr_reader :wh_id

  def initialize(listing_ids, mode)
    @listing_ids = listing_ids
    @mode = mode
  end

  def mode_check
    if mode == :quantity || mode == :simple || mode == :active
      quantity_simple_mode
    elsif mode == :create || mode == :item_create || mode == :update
      create_mode
    elsif mode == :map_create
      map_create_mode
    else
      default
    end
  end

  def quantity_simple_mode
    listings.map do |listing|
      param =
        if listing.channel_id == 18
          qty_simple_params(listing)
            .merge(zilingo_quantity: zilingo_quantity)
        elsif listing.channel_id == 13
          qty_simple_params(listing)
            .merge(zalora_reserved_stock: zalora_reserved_stock)
        elsif listing.channel_id == 2
          qty_simple_params(listing)
            .merge({shopify_inventory_location: shopify_inventory_location})
        elsif listing.channel_id == 3
          qty_simple_params(listing)
            .merge({lazada_quantity: lazada_quantity})
        else
          qty_simple_params(listing)
        end
      modes[mode].new(param).perform
    end.compact
  end

  def qty_simple_params(listing)
    {
      listing: listing, wh_spaces: wh_spaces, variants: variants,
      stock_allocs: stock_allocs, variant_listings: variant_listings,
      bundles: bundles, item_bundle_variants: item_bundle_variants,
      existing_alloc_stocks: existing_alloc_stocks, wh_id: wh_id
    }
  end

  def create_mode
    item_listings.map do |listing|
      param =
        if listing.channel_id == 12
          qty_simple_params(listing)
            .merge(shipping_providers: shipping_providers)
        else
          qty_simple_params(listing)
        end
      modes[mode].new(param).perform
    end
  end

  def map_create_mode
    listings.map do |listing|
      modes[mode].new(qty_simple_params(listing)).perform
    end
  end

  def default
    datas =
      if mode == :precondition_create || mode == :precondition_update
        item_listings
      else
        listings
      end

    datas.map do |listing|
      modes[mode].new(listing: listing).perform
    end
  end

  def existing_alloc_stocks
    @existing_alloc_stocks ||= VariantListingStockAllocation.where(
      variant_association_id: vl_ids
    ).where('end_at >= NOW()')
  end

  def vl_ids
    @vl_ids ||= variant_listings.map(&:id).uniq
  end

  def variant_listings
    @variant_listings ||= VariantListing.where(variant_id: variant_ids)
  end

  def item_bundle_variants
    @item_bundle_variants ||= BundleVariant.where(
      bundle_id: bundle_ids
    )
  end

  def bundle_ids
    @bundle_ids ||= bundles.map(&:id).uniq
  end

  def bundles
    @bundles ||= Bundle.where(variant_id: variant_ids).group(:variant_id)
  end

  def stock_allocs
    @stock_allocs ||= VariantListingStockAllocation.where(
      variant_association_id: vl_ids
    )
  end

  def wh_spaces
    @wh_spaces ||= WarehouseSpace.where(item_variant_id: variant_ids).where.not(warehouse_id: nil)
  end

  def variants
    @variants ||= Variant.where(id: variant_ids)
  end

  def variant_ids
    @variant_ids ||= listings.map(&:variant_id).uniq
  end

  def listings
    @listings ||= VariantListing.joins(:variant).where(id: listing_ids)
  end

  def item_listings
    @item_listings ||= VariantListing.joins(:variant).includes(:item_listing).where(
      id: listing_ids
    ).group(:channel_association_id)
  end

  def shipping_providers
    @shipping_providers ||= ShopeeShippingProvider.where(
      enabled: true, mask_channel_id: 0, account_id: account_id
    )
  end

  def skus
    @skus ||= listings.map(&:local_id).uniq
  end

  def channel_association_ids
    @listing_item_ids ||= listings.map(&:channel_association_id).uniq
  end

  def zilingo_quantity
    @zilingo_quantity ||= ItemBuilder::ZilingoQuantityService.new(
      listings: listings, skus: skus
    ).perform
  end

  def zalora_reserved_stock
    @zalora_reserved_stock ||= ItemBuilder::ZaloraQuantityService.new(
      listings: listings, skus: skus
    ).perform
  end

  def shopify_inventory_location
    @shopify_inventory_location ||= ItemBuilder::ShopifyQuantityService.new(
      listings: listings, skus: skus
    ).perform
  end

  def lazada_quantity
    @lazada_quantity ||= nil
  end

  def modes
    {
      price: ItemBuilder::Modes::PriceService,
      quantity: ItemBuilder::Modes::QuantityService,
      simple: ItemBuilder::Modes::SimpleService,
      active: ItemBuilder::Modes::ActiveService,
      update: ItemBuilder::Modes::CreateService,
      create: ItemBuilder::Modes::CreateService,
      map_create: ItemBuilder::Modes::MapCreateService,
      precondition_create: ItemBuilder::Modes::CreateService,
      precondition_update: ItemBuilder::Modes::CreateService,
      item_create: ItemBuilder::Modes::CreateService,
    }
  end

  def wh_mapping
    @wh_mapping ||= WarehouseMapping.joins(:warehouse).where(
      profile_channel_association_id: account_id
    ).where(warehouses: {consignment: false}).first
  end

  def account_id
    account_id ||= listings[0].profile_channel_association_id
  end

  def wh_id
    @wh_id ||= wh_mapping&.warehouse_id
  end
end
