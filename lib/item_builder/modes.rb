# frozen_string_literal: true

require 'item_builder/modes/base_service'
require 'item_builder/modes/price_service'
require 'item_builder/modes/quantity_service'
require 'item_builder/modes/simple_service'
require 'item_builder/modes/active_service'
require 'item_builder/modes/update_service'
class ItemBuilder
  module Modes
    attr_reader :listing
    attr_reader :wh_spaces
    attr_reader :variants
    attr_reader :stock_allocs
    attr_reader :bundles
    attr_reader :item_bundle_variants
    attr_reader :existing_alloc_stocks
    attr_reader :variant_listings
    attr_reader :zilingo_quantity
    attr_reader :lazada_quantity
    attr_reader :zalora_reserved_stock
    attr_reader :shopify_inventory_location
    attr_reader :wh_id
    attr_reader :shipping_providers
    def initialize(args)
      @listing = args.fetch(:listing)
      @wh_spaces = args.fetch(:wh_spaces, [])
      @variants = args.fetch(:variants, [])
      @stock_allocs = args.fetch(:stock_allocs, [])
      @bundles = args.fetch(:bundles, [])
      @item_bundle_variants = args.fetch(:item_bundle_variants, [])
      @existing_alloc_stocks = args.fetch(:existing_alloc_stocks, [])
      @variant_listings = args.fetch(:variant_listings, [])
      @zilingo_quantity = args.fetch(:zilingo_quantity, [])
      @lazada_quantity = args.fetch(:lazada_quantity, [])
      @zalora_reserved_stock = args.fetch(:zalora_reserved_stock, [])
      @shopify_inventory_location = args.fetch(:shopify_inventory_location, [])
      @wh_id = args.fetch(:wh_id, [])
      @shipping_providers = args.fetch(:shipping_providers, [])
    end

    def base
      {
        id: listing.id,
        local_id: listing.local_id,
        local_item_id: listing.local_item_id,
        sku: listing.sku,
        variant_id: listing.variant_id
      }
    end

    def existing_allocated_stock
      existing_alloc_stocks.map do |els|
        if listings.pluck(:id).include?(els.variant_association_id)
          els
        end
      end.compact
    end

    def listings
      variant_listings.select { |vl| vl.variant_id == listing.variant_id}
    end

    def bundle_variants
      if bundle.present?
        item_bundle_variants.select { |ibv| ibv.bundle_id == bundle.id }
      end
    end

    def bundle
      bundles.select { |b| b.variant_id == listing.variant_id }.first
    end

    def stock_alloc
      stock_allocs.select { |sa| sa.variant_association_id == listing.id }.first
    end

    def warehouse
      if wh_id.present?
        wh_spaces.where(warehouse_id: wh_id).where(item_variant_id: listing.variant_id).first
      else
        wh_spaces.select { |ws| ws.item_variant_id == listing.variant_id }.first
      end
    end

    def variant
      variants.select { |v| v.id == listing.variant_id }.first
    end
  end
end
