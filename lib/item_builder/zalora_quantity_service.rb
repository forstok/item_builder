# frozen_string_literal: true

require 'item_builder/modes.rb'
class ItemBuilder
  class ZaloraQuantityService
    attr_reader :listings, :skus
    def initialize(args)
      @listings = args.fetch(:listings)
      @skus = args.fetch(:skus)
    end

    def perform
      args = {
        method: :post, url: url, payload: data, headers: headers
      }
      resp = JSON.parse(rest_client(args, [200, 500, 406]))
      response_process(resp)
    end

    def headers
      { content_type: :json, accept: :json }
    end

    def data
      {
        "credential": JSON.parse(credential)['credential'],
        "data": { "skus": skus }
      }.to_json
    end

    def credential
      return @credential if @credential

      account_id = listings[0].profile_channel_association_id
      host = ENV['CREDENTIAL_URL'] || raise('credential url is not set')
      url = "#{host}/credential?account_id=#{account_id}"
      @credential = rest_client(method: :get, url: url)
    end

    def url
      url = ENV['API_GATEWAY_URL'] || raise('api gateway is not set')
      url + '/zalora/product_stocks'
    end

    def response_process(resp)
      if resp.dig('response_code') && resp.dig('response_code') != 200
        raise "Response Code is #{resp.dig('response_code')}"
      elsif resp.dig('ErrorResponse', 'Head', 'ErrorMessage') == 'E009: Access Denied'
        return nil
      elsif resp.dig('ErrorResponse').present?
        return nil
      end

      success_handle(resp)
    end

    def success_handle(resp)
      hash = {}
      resp.dig('SuccessResponse', 'Body', 'ProductStocks', 'ProductStock').each do |sku|
        hash[sku['SellerSku']] = sku['ReservedStock']
      end if resp.dig('SuccessResponse').present?
      hash
    end

    def rest_client(params, rescued_codes = 200)
      RestClient::Request.execute(params.merge(timeout: 3)) do |response|
        code = response.code
        unless Array.wrap(rescued_codes).include?(code)
          raise "Response Code is #{code}"
        end

        response
      end
    end
  end
end
