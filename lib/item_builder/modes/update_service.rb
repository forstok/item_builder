# frozen_string_literal: true

require 'item_builder/modes.rb'
require 'item_builder/modes/update/blibli_service'
require 'item_builder/modes/update/bukalapak_service'
require 'item_builder/modes/update/lazada_service'
require 'item_builder/modes/update/zalora_service'
require 'item_builder/modes/update/shopify_service'
require 'item_builder/modes/update/jd_service'
require 'item_builder/modes/update/tokopedia_service'
class ItemBuilder
  module Modes
    class UpdateService
      include Modes

      UPDATE_CHANNEL = {}.tap do |hash|
        hash[2]       = :Shopify
        hash[3]       = :Lazada
        hash[4]       = :Bhinneka
        hash[5]       = :Blanja
        hash[9]       = :Blibli
        hash[11]      = :Bukalapak
        hash[12]      = :Shopee
        hash[13]      = :Zalora
        hash[15]      = :Tokopedia
        hash[16]      = :Jd
      end.freeze

      def perform
        to_h.merge(base)
      end

      def to_h
        simple.merge(update)
      end

      def update
        class_name = "ItemBuilder::Modes::Update::#{channel_name}Service"
        update_channel_service = class_name.constantize
        update_channel_service.new(listing, item_listing).perform
      end

      def channel_name
        UPDATE_CHANNEL[listing.channel_id].to_s
      end

      def simple
        SimpleService.new(listing: listing).to_h
      end

      def item_listing
        @item_listing ||= ItemListing.find(listing.channel_association_id)
      end
    end
  end
end
