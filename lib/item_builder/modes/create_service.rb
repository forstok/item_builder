# frozen_string_literal: true

require 'item_builder/modes.rb'
require 'item_builder/modes/create/shopee_service'
require 'item_builder/modes/create/tokopedia_service'
class ItemBuilder
  module Modes
    class CreateService
      include Modes

      CREATE_CHANNEL = {}.tap do |hash|
        hash[12]      = :Shopee
        hash[15]      = :Tokopedia
      end.freeze

      def perform
        to_h.merge(create_channel)
      end

      def to_h
        {
          item: {
            id: item_listing.id,
            variations: ::ItemBuilder.build(listings.map(&:id), :map_create)
          }
        }
      end

      def create_channel
        class_name = "ItemBuilder::Modes::Create::#{channel_name}Service"
        create_channel_service = class_name.constantize
        create_channel_service.new(item_listing, shipping_providers).perform
      end

      def channel_name
        CREATE_CHANNEL[listing.channel_id].to_s
      end

      def item_listing
        @item_listing ||= listing.item_listing
      end

      def listings
        @listings ||= item_listing.variant_listings.where(
          profile_channel_association_id: listing.profile_channel_association_id
        )
      end
    end
  end
end
