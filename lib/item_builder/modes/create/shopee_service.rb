# frozen_string_literal: true

require 'item_builder/modes/create/base'
class ItemBuilder
  module Modes
    module Create
      class ShopeeService < Base
        def perform
          {
            category: category,
            brand: brand,
            shipping_providers: map_shipping_providers
          }
        end

        def category
          {
            id: item_listing&.item_listing_category&.id,
            local_id: item_listing&.item_listing_category&.shopee&.local_id,
            name: item_listing&.item_listing_category&.shopee&.name
          }
        end

        def brand
          {
            id: item_listing&.item_listing_brand&.id,
            local_id: item_listing&.item_listing_brand&.shopee&.local_id,
            name: item_listing&.item_listing_brand&.shopee&.name
          }
        end

        def map_shipping_providers
          shipping_providers.map do |shipping_provider|
            {
              local_id: shipping_provider.local_id,
              name: shipping_provider.name,
              enabled: shipping_provider.enabled
            }
          end
        end
      end
    end
  end
end
