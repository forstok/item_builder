# frozen_string_literal: true

class ItemBuilder
  module Modes
    module Create
      class Base
        attr_reader :item_listing
        attr_reader :shipping_providers

        def initialize(item_listing, shipping_providers)
          raise 'item listing is not set' if item_listing.nil?

          raise 'shipping providers is not set' if shipping_providers.nil?

          @item_listing = item_listing
          @shipping_providers = shipping_providers
        end
      end
    end
  end
end
