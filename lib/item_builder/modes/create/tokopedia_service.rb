# frozen_string_literal: true

require 'item_builder/modes/create/base'
class ItemBuilder
  module Modes
    module Create
      class TokopediaService < Base
        def perform
          {
            category: category,
            master_option_types: master_option_types
          }
        end

        def category
          {
            id: item_listing&.item_listing_category&.id,
            local_id: item_listing&.item_listing_category&.tokopedia&.local_id,
            name: item_listing&.item_listing_category&.tokopedia&.name
          }
        end

        def master_option_types
          item.option_types.map do |option_type|
            {
              name: option_type.name,
              option_list: option_list(option_type)
            }
          end
        end

        private

        def option_list(option_type)
          options = option_type
            .options
            .joins(:variant_option_associations)
            .where({item_variant_option_associations: {variant_id: variant_ids}})
            .pluck(:name)
            .reject{|option_name| option_name.blank? }
            .map{|name| YAML.load(name)}
        end

        def variant_ids
          @variant_ids ||= item.variants.pluck(:id)
        end

        def item
          @item ||= item_listing.item
        end
      end
    end
  end
end
