# frozen_string_literal: true

require 'item_builder/modes.rb'
class ItemBuilder
  module Modes
    class BaseService
      attr_reader :listing
      def initialize(args)
        @listing = args.fetch(listing)
      end

      def perform
        {
          id: listing.id,
          local_id: listing.local_id,
          local_item_id: listing.local_item_id,
          sku: listing.sku
        }
      end
    end
  end
end
