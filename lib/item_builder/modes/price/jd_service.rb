# frozen_string_literal: true

require 'date'
require 'item_builder/modes/price/base'
class ItemBuilder
  module Modes
    module Price
      class JdService < Base
        def perform
          {
            price: listing.price,
            sale_price: jd_price,
            sale_start_at: listing.sale_start_at,
            sale_end_at: listing.sale_end_at
          }
        end

        def jd_price
          increment_price =
            if (listing.price % 1000).positive?
              1000
            else
              0
            end
          (listing.price.to_i / 1000) * 1000 + increment_price
        end
      end
    end
  end
end
