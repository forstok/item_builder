# frozen_string_literal: true

require 'date'
require 'item_builder/modes/price/base'
class ItemBuilder
  module Modes
    module Price
      class ShopifyService < Base
        def perform
          {
            price: listing.price,
            sale_price: sale_price,
            sale_start_at: listing.sale_start_at,
            sale_end_at: listing.sale_end_at
          }
        end

        private

        def sale_price
          sale_price_policy.on_sale? ? listing.sale_price : listing.price
        end
      end
    end
  end
end
