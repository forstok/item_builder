# frozen_string_literal: true

require 'date'
require 'item_builder/modes/price/base'
class ItemBuilder
  module Modes
    module Price
      class BukalapakService < Base
        def perform
          if listing.sale_price.nil?
            bukalapak_price
          else
            deal
          end
        end

        def bukalapak_price
          {
            price: listing.price,
            sale_price: listing.sale_price,
            sale_start_at: listing.sale_start_at,
            sale_end_at: listing.sale_end_at
          }
        end

        def deal
          {
            percentage: deal_percentage,
            sale_start_at: sale_start_at,
            sale_end_at: sale_end_at,
            price: listing.price,
            sale_price: listing.sale_price
          }
        end

        def deal_percentage
          price = listing.price.to_f
          sale_price = listing.sale_price.to_f
          100 - (sale_price / price * 100)
        end

        def sale_start_at
          return DateTime.now if listing.sale_start_at.nil?
          return DateTime.now if listing.sale_start_at < DateTime.now

          listing.sale_start_at
        end

        def sale_end_at
          month_later = DateTime.now + 1.month
          return month_later if listing.sale_end_at.nil?
          return month_later if listing.sale_end_at > month_later
          return month_later if listing.sale_end_at < DateTime.now

          listing.sale_end_at
        end
      end
    end
  end
end
