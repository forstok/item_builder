# frozen_string_literal: true

require 'date'
require 'item_builder/modes.rb'
require 'item_builder/modes/price_service'
class ItemBuilder
  module Modes
    module Price
      class SalePricePolicy
        include Modes
        def sale_price
          # has sale price but no sale date defined
          # then push sale price immediately
          # assuming it starts today and no end date specified
          return listing.sale_price if sale_price? && !sale_date?

          # has sale price and today is on sale then push sale price immediately
          listing.sale_price if sale_price? && on_sale?
          # don't push / don't return anything if 2 rules above unfulfilled
        end

        def sale_price?
          listing.sale_price
        end

        def sale_date?
          listing.sale_start_at && listing.sale_end_at
        end

        def on_sale?
          return false unless sale_price?

          sale_start_at = listing.sale_start_at || DateTime.now
          sale_end_at = listing.sale_end_at || DateTime.now + 1_000_000.years
          sale_start_at <= DateTime.now && sale_end_at >= DateTime.now
        end
      end
    end
  end
end
