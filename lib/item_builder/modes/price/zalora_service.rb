# frozen_string_literal: true

require 'date'
require 'item_builder/modes/price/base'
class ItemBuilder
  module Modes
    module Price
      class ZaloraService < Base
        def perform
          {
            price: listing.price,
            sale_price: sale_price_policy.sale_price,
            sale_start_at: listing.sale_start_at,
            sale_end_at: listing.sale_end_at
          }
        end
      end
    end
  end
end
