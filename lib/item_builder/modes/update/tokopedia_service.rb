# frozen_string_literal: true

require 'item_builder/modes/update/base'
class ItemBuilder
  module Modes
    module Update
      class TokopediaService < Base
        def perform
          tokopedia_attr.merge(base)
        end

        def tokopedia_attr
          {
            condition: (listing.new_condition ? 'NEW' : 'USED')
          }
        end

        def possible_variant_options
          item_listing.item_listing_custom_fields
            .where.not(value: '') # prevent unselected/empty options
            .group_by(&:tokopedia_custom_field_id)
            .each_with_index.map {|variant_custom_fields, index1|
              variant_custom_field_sample = variant_custom_fields.first
              selected_tokopedia_custom_field = tokopedia_variant(variant_custom_field_sample)
              {
                v: variant_custom_field_sample.tokopedia.local_id.to_i,
                vu: selected_tokopedia_custom_field['unit_id'],
                pos: index1 + 1,
                opt: variant_custom_fields.pluck(:value).uniq.map {|custom_field_value|
                  # handle variant with manual value (without options)
                  if variant_custom_field_sample.tokopedia.input_type == 'text'
                    {
                      value: custom_field_value,
                      t_id: custom_field_value.bytes.sum, # set value act as option id
                      cstm: ''
                    }
                  else
                    custom_field_option = variant_custom_field_sample.tokopedia.options
                      .find_by(name: custom_field_value)
                    option_value =
                      if selected_tokopedia_custom_field['unit_id'] == 0
                        custom_field_option.name
                      else
                        custom_field_option.name
                          .split(" #{selected_tokopedia_custom_field['unit']}")
                          .first
                      end
                      {
                        vuv: custom_field_option.local_id, # variant unit value id
                        value: option_value, # original option value
                        t_id: custom_field_option.id,
                        cstm: ''
                      }
                  end
                }
              }
            }
        end
      end
    end
  end
end
