# frozen_string_literal: true

require 'item_builder/modes.rb'
class ItemBuilder
  module Modes
    module Update
      class Base
        attr_reader :listing
        attr_reader :item_listing

        def initialize(listing, item_listing)
          raise 'listing is not set' if listing.nil?

          raise 'item listing is not set' if item_listing.nil?

          @listing = listing
          @item_listing = item_listing
        end

        def base
          {
            name: listing.name,
            minimum_order: listing.minimum_order,
            package_weight: listing.package_weight
          }
        end
      end
    end
  end
end
