# frozen_string_literal: true

require 'item_builder/modes/quantity_service.rb'
require 'item_builder/modes.rb'
class ItemBuilder
  module Modes
    class ActiveService
      include Modes

      def perform
        to_h.merge(base).merge(quantity)
      end

      def to_h
        {
          active: listing.active,
          active_variant: active_variant.count
        }
      end

      def quantity
        QuantityService.new(
          listing: listing, wh_spaces: wh_spaces, variants: variants,
          stock_allocs: stock_allocs, variant_listings: variant_listings,
          bundles: bundles, item_bundle_variants: item_bundle_variants,
          existing_alloc_stocks: existing_alloc_stocks,
          zilingo_quantity: zilingo_quantity,
          zalora_reserved_stock: zalora_reserved_stock
        ).to_h
      end

      def active_variant
        VariantListing
          .where(local_item_id: listing.local_item_id,
                 active: 1)
      end
    end
  end
end
