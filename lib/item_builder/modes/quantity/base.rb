# frozen_string_literal: true

class ItemBuilder
  module Modes
    module Quantity
      class Base
        attr_reader :listing
        attr_reader :available_quantity
        attr_reader :local_qty

        def initialize(listing, available_quantity, local_qty)
          raise 'listing is not set' if listing.nil?

          @listing = listing
          @available_quantity = available_quantity
          @local_qty = local_qty
        end
      end
    end
  end
end
