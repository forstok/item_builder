# frozen_string_literal: true

require 'item_builder/modes/quantity/base'
class ItemBuilder
  module Modes
    module Quantity
      class LazadaService < Base
        def perform
          available_quantity
        end
      end
    end
  end
end
