# frozen_string_literal: true

require 'item_builder/modes/quantity/base'
class ItemBuilder
  module Modes
    module Quantity
      class ZilingoService < Base
        def perform
          available_quantity - local_qty
        end
      end
    end
  end
end
