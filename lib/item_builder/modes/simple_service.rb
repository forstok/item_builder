# frozen_string_literal: true

require 'item_builder/modes/quantity_service.rb'
require 'item_builder/modes/price_service.rb'
require 'item_builder/modes.rb'
class ItemBuilder
  module Modes
    class SimpleService
      include Modes

      def perform
        to_h.merge(base)
      end

      def to_h
        quantity.merge(price)
      end

      def quantity
        QuantityService.new(
          listing: listing, wh_spaces: wh_spaces, variants: variants,
          stock_allocs: stock_allocs, variant_listings: variant_listings,
          bundles: bundles, item_bundle_variants: item_bundle_variants,
          existing_alloc_stocks: existing_alloc_stocks,
          zilingo_quantity: zilingo_quantity
        ).to_h
      end

      def price
        PriceService.new(listing: listing).to_h
      end
    end
  end
end
