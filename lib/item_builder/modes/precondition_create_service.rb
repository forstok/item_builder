# frozen_string_literal: true

require 'item_builder/modes.rb'
require 'item_builder/modes/precondition_create/shopee_service'
class ItemBuilder
  module Modes
    class PreconditionCreateService
      include Modes

      MAP_PRECONDITION_CREATE_CHANNEL = {}.tap do |hash|
        hash[12]      = :Shopee
      end.freeze

      def perform
        base.merge(map_precondition_create)
      end

      def map_precondition_create
        class_name = "ItemBuilder::Modes::PreconditionCreate::#{channel_name}Service"
        create_channel_service = class_name.constantize
        create_channel_service.new(listing).perform
      end

      def channel_name
        MAP_PRECONDITION_CREATE_CHANNEL[listing.channel_id].to_s
      end
    end
  end
end
