# frozen_string_literal: true

require 'warehouse_models'
require 'item_builder/modes.rb'
require 'item_builder/get_quantity_service'
require 'item_builder/modes/quantity/base'
require 'item_builder/modes/quantity/lazada_service'
require 'item_builder/modes/quantity/zalora_service'
require 'item_builder/modes/quantity/zilingo_service'
class ItemBuilder
  module Modes
    class QuantityService
      include Modes

      QUANTITY_CHANNEL = {}.tap do |hash|
        hash[2]       = :Shopify
        hash[3]       = :Lazada
        hash[13]      = :Zalora
        hash[18]      = :Zilingo
      end.freeze

      def perform
        if channel_name == "Shopify"
          dataSIL = shopify_inventory_location[listing.local_id]

          base.merge(to_h, dataSIL) if dataSIL.present?
        else
          to_h.merge(base)
        end
      end

      def to_h
        if channel_name == "Zilingo"
          { quantity: qty }
        else
          {
            quantity: [qty, 0].sort[1]
          }
        end
      end

      def qty
        if channel_name.empty? || channel_name == "Shopify"
          available_quantity
        else
          qty_channel
        end
      end

      def qty_channel
        class_name = "ItemBuilder::Modes::Quantity::#{channel_name}Service"
        qty_channel_service = class_name.constantize
        qty_channel_service.new(listing, available_quantity, local_qty.to_i).perform
      end

      def channel_name
        QUANTITY_CHANNEL[listing.channel_id].to_s
      end

      def local_qty
        if channel_name == 'Zilingo'
          return 0 if zilingo_quantity.blank?

          zilingo_quantity[listing.local_id].to_i
        elsif channel_name == 'Zalora'
          return 0 if zalora_reserved_stock.blank?

          zalora_reserved_stock[listing.local_id].to_i
        else
          return 0 if lazada_quantity.blank?

          lazada_quantity[listing.local_id].to_i
        end
      end

      def available_quantity
        ItemBuilder::GetQuantityService.new(
          listing: listing, variant: variant,
          wh_sp: warehouse, stock_alloc: stock_alloc,
          bundle_variants: bundle_variants,
          existing_allocated_stock: existing_allocated_stock
        ).perform
      end
    end
  end
end
