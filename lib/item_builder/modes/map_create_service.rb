# frozen_string_literal: true

require 'item_builder/modes.rb'
require 'item_builder/modes/map_create/shopee_service'
require 'item_builder/modes/map_create/tokopedia_service'
class ItemBuilder
  module Modes
    class MapCreateService
      include Modes

      MAP_CREATE_CHANNEL = {}.tap do |hash|
        hash[12]      = :Shopee
        hash[15]      = :Tokopedia
      end.freeze

      def perform
        base.merge(simple(listing)).merge(to_h).merge(map_create_channel)
      end

      def simple(listing)
        SimpleService.new(
          listing: listing, wh_spaces: wh_spaces, variants: variants,
          stock_allocs: stock_allocs, variant_listings: variant_listings,
          bundles: bundles, item_bundle_variants: item_bundle_variants,
          existing_alloc_stocks: existing_alloc_stocks,
          zilingo_quantity: zilingo_quantity,
          zalora_reserved_stock: zalora_reserved_stock
        ).to_h
      end

      def to_h
        {
          name: listing.name,
          description: listing.description,
          weight: listing.package_weight,
          length: listing.package_length,
          width: listing.package_width,
          height: listing.package_height,
          condition: listing.new_condition,
          option_name: listing.option_name,
          option_value: listing.option_value,
          option2_name: listing.option2_name,
          option2_value: listing.option2_value,
          internal_data: listing.internal_data,
          images: images
        }
      end

      def images
        listing.item_listing_variant_images.map do |img|
          {
            id: img.id,
            local_id: img.local_id,
            local_url: img.local_url,
            url: img.image.image.url(:xlarge, timestamp: false)
          }
        end
      end

      def map_create_channel
        class_name = "ItemBuilder::Modes::MapCreate::#{channel_name}Service"
        create_channel_service = class_name.constantize
        create_channel_service.new(listing).perform
      end

      def channel_name
        MAP_CREATE_CHANNEL[listing.channel_id].to_s
      end
    end
  end
end
