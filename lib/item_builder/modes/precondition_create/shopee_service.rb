# frozen_string_literal: true

require 'item_builder/modes/precondition_create/base'
class ItemBuilder
  module Modes
    module PreconditionCreate
      class ShopeeService < Base
        def perform
          {
            internal_data: listing.internal_data,
            images: images
          }
        end

        def images
          listing.item_listing_variant_images.map do |img|
            {
              id: img.id,
              local_id: img.local_id,
              local_url: img.local_url,
              url: img.image.image.url(:xlarge, timestamp: false)
            }
          end
        end
      end
    end
  end
end
