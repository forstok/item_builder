# frozen_string_literal: true

require 'item_builder/modes/map_create/base'
class ItemBuilder
  module Modes
    module MapCreate
      class TokopediaService < Base
        def perform
          {
            item_custom_fields: [],
            variant_custom_fields: variant_custom_fields,
            new_condition: listing.new_condition,
            minimum_order: listing.minimum_order,
            etalase_id: listing.tokopedia_showcase&.local_id,
            master_option_types: master_option_types,
            images: images
          }
        end

        private

        def variant_custom_fields
          listing.item_listing_variant_custom_fields.map do |variant|
            {
              value: YAML.load(variant.value),
              local_id: variant&.tokopedia&.local_id,
              name: variant&.tokopedia&.name,
              identifier: variant&.tokopedia&.display_name
            }
          end
        end

        def master_option_types
          listing
            .variant
            .options.order(:option_type_id)
            .pluck(:name)
            .map{|name| YAML.load(name)}
        end

        def images
          listing.item_listing_variant_images.map do |img|
            {
              id: img.id,
              local_id: img.local_id,
              local_url: img.local_url,
              url: img.image.image.url(:xlarge, timestamp: false)
            }
          end
        end
      end
    end
  end
end
