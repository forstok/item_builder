# frozen_string_literal: true

class ItemBuilder
  module Modes
    module MapCreate
      class Base
        attr_reader :listing

        def initialize(listing)
          raise 'listing is not set' if listing.nil?

          @listing = listing
        end
      end
    end
  end
end
