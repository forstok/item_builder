# frozen_string_literal: true

require 'item_builder/modes/map_create/base'
class ItemBuilder
  module Modes
    module MapCreate
      class ShopeeService < Base
        def perform
          {
            item_custom_fields: item_custom_fields,
            variant_custom_fields: variant_custom_fields,
            shipping_providers: shipping_providers
          }
        end

        def item_custom_fields
          listing.item_listing_custom_fields.map do |item|
            next unless available_custom_field_ids.include?(item&.shopee&.id)

            {
              value: item.value,
              local_id: item&.shopee&.local_id,
              data_type: item&.shopee&.data_type,
              input_type: item&.shopee&.input_type,
              options: item&.shopee&.options.map do |option|
                {
                  id: option.id,
                  name: option.name,
                  local_id: option.local_id
                }
              end
            }
          end.compact
        end
  
        def variant_custom_fields
          listing.item_listing_variant_custom_fields.map do |variant|
            {
              value: variant.value,
              local_id: variant&.shopee&.local_id,
              data_type: variant&.shopee&.data_type,
              input_type: variant&.shopee&.input_type
            }
          end
        end

        def shipping_providers
          listing.shopee_shipping_providers.map do |shipping_provider|
            {
              logistic_id: shipping_provider['logistic_id']
            }
          end
        end

        private

        def shopee_category_id
          @shopee_category_id ||= listing.item_listing.item_listing_category.shopee_category_id
        end

        def available_custom_field_ids
          @available_custom_field_ids ||= ::ShopeeItemCustomFieldCategoryAssociation
            .where(category_id: shopee_category_id)
            .pluck(:custom_field_id)
        end
      end
    end
  end
end
