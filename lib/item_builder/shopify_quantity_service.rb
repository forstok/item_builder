# frozen_string_literal: true

require 'item_builder/modes.rb'
class ItemBuilder
  class ShopifyQuantityService
    attr_reader :listings, :skus
    def initialize(args)
      @listings = args.fetch(:listings)
      @skus = args.fetch(:skus)
      @account_id = listings[0].profile_channel_association_id
      @variant_listings = {}
    end

    def perform
      datas = {}
      @listings.each do |listing|
        if listing.local_id.present?
          @listing = listing
          @sku = listing.local_id
          resp = response_process
          next if resp[:inventory_item_id].nil? || resp[:inventory_item_id] == 0

          datas[@sku] = resp
        end
      end

      # update variant real_local_id database icava
      VariantListing.update(@variant_listings.keys, @variant_listings.values)
      datas
    end

    def inventory_item_id
      real_local_id = @listing.real_local_id
      listing_id = @listing.id

      if real_local_id.nil? || real_local_id.blank?
        # get data from shopify
        args = {method: :post, url: url_variant, payload: data_variant, headers: headers}

        resp = JSON.parse(rest_client(args, [200, 402]))

        hash = { listing_id => { "real_local_id" => resp['inventory_item_id'] } }
        @variant_listings = @variant_listings.merge(hash)

        resp['inventory_item_id']
      else
        # real_local_id exists
        real_local_id
      end
    end

    def location_id
      cred = JSON.parse(credential)['credential']
      if cred['primary_warehouse_id'].present?
        cred['primary_warehouse_id']
      else
        # get data from shopify
        args = {
          method: :post, url: url_location, payload: data_location, headers: headers
        }

        resp = JSON.parse(rest_client(args, [200, 402]))
        resp['id']
      end
    end

    def headers
      { content_type: :json, accept: :json }
    end

    def data_location
      {
        "credential": JSON.parse(credential)['credential']
      }.to_json
    end

    def data_variant
      {
        "credential": JSON.parse(credential)['credential'],
        "data": { "local_id": @sku }
      }.to_json
    end

    def credential
      return @credential if @credential

      host = ENV['CREDENTIAL_URL'] || raise('credential url is not set')
      url = "#{host}/credential?account_id=#{@account_id}"
      @credential = rest_client(method: :get, url: url)
    end

    def url_variant
      url = ENV['API_GATEWAY_URL'] || raise('api gateway is not set')
      url + "/shopify/data_variant"
    end

    def url_location
      url = ENV['API_GATEWAY_URL'] || raise('api gateway is not set')
      url + "/shopify/data_location"
    end

    def response_process
      hash = Hash.new

      begin
        hash[:inventory_item_id] = inventory_item_id.to_i
        hash[:location_id] =  location_id.to_i
      rescue
        hash
      end

      hash
    end

    def rest_client(params, rescued_codes = 200)
      RestClient::Request.execute(params.merge(timeout: 3)) do |response|
        code = response.code
        resp = response.body.to_str
        unless Array.wrap(rescued_codes).include?(code)
          raise "Response Code is #{code}" unless resp.include?('Response code = 404')
        end

        response
      end
    end
  end
end
