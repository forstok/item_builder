# frozen_string_literal: true

require 'item_builder/modes.rb'
class ItemBuilder
  class LazadaQuantityService
    attr_reader :listings, :skus
    def initialize(args)
      @listings = args.fetch(:listings)
      @skus = args.fetch(:skus)
    end

    def perform
      args = {
        method: :post, url: url, payload: data, headers: headers
      }
      resp = JSON.parse(rest_client(args, [200, 500, 406]))
      response_process(resp)
    end

    def headers
      { content_type: :json, accept: :json }
    end

    def data
      {
        "credential": JSON.parse(credential)['credential'],
        "data": { "skus": skus.to_json }
      }.to_json
    end

    def credential
      return @credential if @credential

      account_id = listings[0].profile_channel_association_id
      host = ENV['CREDENTIAL_URL'] || raise('credential url is not set')
      url = "#{host}/credential?account_id=#{account_id}"
      @credential = rest_client(method: :get, url: url)
    end

    def url
      url = ENV['API_GATEWAY_URL'] || raise('api gateway is not set')
      url + '/lazada/items'
    end

    def response_process(resp)
      hash = {}
      resp['data']['products'].each do |product|
        product['skus'].each do |sku|
          qty = sku['multiWarehouseInventories'][0]
          hash[sku['SellerSku']] = qty['occupyQuantity'] + qty['withholdQuantity']
        end
      end if resp['data'].present?
      hash
    end

    def rest_client(params, rescued_codes = 200)
      RestClient::Request.execute(params.merge(timeout: 3)) do |response|
        code = response.code
        unless Array.wrap(rescued_codes).include?(code)
          raise "Response Code is #{code}"
        end

        response
      end
    end
  end
end
