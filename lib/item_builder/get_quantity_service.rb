# frozen_string_literal: true

require 'warehouse_models'
require 'item_builder/modes.rb'
class ItemBuilder
  class GetQuantityService
    attr_reader :listing, :wh_sp, :variant
    def initialize(args)
      @listing = args.fetch(:listing)
      @wh_sp = args.fetch(:wh_sp)
      @variant = args.fetch(:variant)
      @stock_alloc = args.fetch(:stock_alloc)
      @bundle_variants = args.fetch(:bundle_variants)
      @existing_allocated_stock = args.fetch(:existing_allocated_stock)
    end

    def perform
      return 0 if check_consignment_variant?

      if allocated_stock_active?
        # yang masuk di kondisi ini,
        # artinya akun tersebut ada allocated stock yang aktif
        allocated_stock
      elsif @existing_allocated_stock.present?
        # yang masuk di kondisi ini,
        # artinya akun tersebut tidak ada allocated stock yang aktif,
        # namun ada allocated stock yg aktif dari channel lain
        warehouse_stock - count_existing_alloc_stock + count_alloc_rsvd_stock
      else
        # yang masuk di kondisi ini,
        # artinya tidak ada allocated stock di item tersebut
        warehouse_stock
      end
    end

    private

    def check_consignment_variant?
      listing.consignment? ||
        (
          !listing.active? && [11, 12, 15, 19, 2, 18].include?(listing.channel_id)
        )
    end

    def allocated_stock_active?
      listing.present? && @stock_alloc.present? && allocated_start_end?
    end

    def allocated_start_end?
      @stock_alloc.start_at <= Time.now &&
        @stock_alloc.end_at >= Time.now
    end

    def allocated_stock
      @stock_alloc.quantity.to_i - one_alloc_rsvd_stock(@stock_alloc).to_i
    end

    # warehouse_stock fungsi untuk mendapatkan available quantity
    # baik itu bundle atau default
    def warehouse_stock
      case variant.config.downcase
      when 'default'
        qty_default
      when 'free gift', 'combo'
        qty_bundle
      else
        raise "config not recognize => #{variant.config}"
      end
    end

    def qty_default
      # set lower limit for quantity
      [listing_wh_sp_quantity, 0].sort[1]
    end

    def qty_bundle
      # Quantity for bundle config
      if @bundle_variants.present?
        qty_list = []
        @bundle_variants.each do |bvr|
          warehouse_space = wh_space.select {|ws| ws.item_variant_id == bvr.variant_id }.first
          qty = warehouse_space.quantity if warehouse_space.present?
          qty ||= 0
          qty = qty / bvr.unit
          qty_list.push(qty)
        end
        [qty_list.min, 0].sort[1]
      else
        qty_default
      end
    end

    def wh_space
      @wh_space ||= WarehouseSpace.where(item_variant_id: variant_ids).where(warehouse_id: wh_sp.warehouse_id)
    end

    def variant_ids
      @variant_ids ||= @bundle_variants.map(&:variant_id).uniq
    end

    def listing_wh_sp_quantity
      @listing_wh_sp_quantity ||= wh_sp&.quantity || 0
    end

    def count_existing_alloc_stock
      return 0 if @existing_allocated_stock.blank?

      @existing_allocated_stock.sum(&:quantity)
    end

    def count_alloc_rsvd_stock
      stock = 0
      @existing_allocated_stock.each do |allocated_stock|
        stock += one_alloc_rsvd_stock(allocated_stock).to_i
      end
      stock
    end

    def host
      url = ENV['ORDERS_URL'] || 'orders.forstok.com'
      url + '/api/v2/item_line/count_one_allocated_reserved_stock'
    end

    def params(allocated_stock)
      {
        channel_id: allocated_stock.variant_listing.channel_id,
        item_variant_id: allocated_stock.variant_listing.variant_id,
        start_at: allocated_stock.start_at,
        end_at: allocated_stock.end_at
      }.to_query
    end

    # one_alloc_rsvd_stock fungsi untuk mendapatkan satu
    # allocated reserved stock
    def one_alloc_rsvd_stock(allocated_stock)
      RestClient.get("#{host}?#{params(allocated_stock)}")
    end
  end
end
