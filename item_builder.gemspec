# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require_relative 'lib/item_builder/version'

Gem::Specification.new do |spec|
  spec.name          = 'item_builder'
  spec.version       = ItemBuilder::VERSION
  spec.authors       = ['okaaryanata']
  spec.email         = ['oka.aryanata9@gmail.com']

  spec.summary       = 'Item builder'
  spec.description   = 'Forstok Gem to build item'
  # spec.homepage      = 'TODO: Put your gem's website or public repo URL here.'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  # if spec.respond_to?(:metadata)
  #   # spec.metadata['allowed_push_host'] = 'http://mygemserver.com'

  #   # spec.metadata['homepage_uri'] = spec.homepage
  #   spec.metadata['source_code_uri'] = 'https://bitbucket.org/forstok/item_builder/src/master/'
  # else
  #   raise 'RubyGems 2.0 or newer is required to protect against ' \
  #     'public gem pushes.'
  # end

  spec.files = [
    Dir['lib/**/*.rb']
  ]
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.17.2'
  spec.add_development_dependency 'byebug', '~> 11.1.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.79.0'
  spec.add_dependency 'item_models_2', '~> 0.0.21'
  spec.add_dependency 'warehouse_models', '~> 0.1.2'
end
