# frozen_string_literal: true

class VariantListing;end
class WarehouseSpace;end
RSpec.describe ItemBuilder do
  subject { described_class }
  let(:listing) { double 'listing', id: 1, sku: 'sku', variant_id: 12, local_id: 'local_id', local_item_id: 'local_item_id', price: 1230, sale_price: 123, sale_start_at: sale_start_at, sale_end_at: sale_end_at, channel_id: 15 }
  let(:listings) { [ id: 1, sku: 'sku', variant_id: 12, local_id: 'local_id', local_item_id: 'local_item_id' ] }
  let(:warehouse) { double 'warehouse', id: 1, warehouse_id: 1, item_variant_id: 12, quantity: 0 }
  let(:variant) { double 'variant', id: 12, item_id: 1, config: 'default' }
  let(:stock_allocation) { double 'stock allocation', id: 1, variant_association_id: 1, quantity: 3, start_at: start_at, end_at: end_at }
  let(:sale_start_at) { Time.now.to_s }
  let(:sale_end_at) { Time.now.to_s }
  let(:start_at) { DateTime.now }
  let(:end_at) { DateTime.now }

  before do
    allow(VariantListing).to receive(:where).with(id: [12]).and_return([listing])
    allow(WarehouseSpace).to receive(:find_by).with(item_variant_id: 12).and_return(warehouse)
    allow(Variant).to receive(:find).with(listing.variant_id).and_return(variant)
    allow(listing).to receive(:consignment?).and_return(false)
    allow(listing).to receive(:active?).and_return(true)
    allow(listing).to receive(:present?).and_return(true)
    allow(listing).to receive(:variant_listing_stock_allocation).and_return(stock_allocation)
    allow(stock_allocation).to receive(:present?).and_return(true)
    allow(stock_allocation).to receive(:start_at).and_return(start_at)
    allow(start_at).to receive(:beginning_of_day).and_return(Time.now)
    allow(stock_allocation).to receive(:end_at).and_return(end_at)
    allow(end_at).to receive(:end_of_day).and_return(Time.now)
    allow(variant).to receive(:variant_listings).and_return(listings)
    allow(listings).to receive(:pluck).with(:id).and_return([1])
    allow(stock_allocation).to receive(:where).with('ADDTIME(end_at, "07:00") >= NOW()').and_return(stock_allocation)
    allow(VariantListingStockAllocation).to receive(:where).with(variant_association_id: [1]).and_return(stock_allocation)
    allow(stock_allocation).to receive(:blank?).and_return(false)
    allow(stock_allocation).to receive(:sum).with(:quantity).and_return(3)
    allow(stock_allocation).to receive(:each).and_return(3)
  end

  describe '#build' do
    context 'when mode is simple' do
      it 'return correct response' do
        expect(ItemBuilder.build([12], :simple)).to be_eql([{:id=>1, :local_id=>"local_id", :local_item_id=>"local_item_id", :price=>1230, :quantity=>-3, :sale_end_at=>sale_end_at, :sale_price=>123, :sale_start_at=>sale_start_at, :sku=>"sku"}])
      end
    end

    context 'when mode is price' do
      it 'return correct response' do
        expect(ItemBuilder.build([12], :price)).to be_eql([{:id=>1, :local_id=>"local_id", local_item_id: 'local_item_id', :price=>1230, :sale_end_at=>sale_end_at, :sale_price=>123, :sale_start_at=>sale_start_at, :sku=>"sku"}])
      end
    end
    context 'when mode is quantity' do
      it 'return correct response' do
        expect(ItemBuilder.build([12], :quantity)).to be_eql([{:id=>1, :local_id=>"local_id", :local_item_id=>"local_item_id", :quantity=>-3, :sku=>"sku"}])
      end
    end
  end
end
