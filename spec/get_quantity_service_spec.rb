# frozen_string_literal: true

class VariantListingStockAllocation;end
require 'date'
RSpec.describe ItemBuilder::GetQuantityService do
  subject { described_class.new(params) }
  let(:params){
    { listing: listing, wh_sp: wh_sp, variant: variant }
  }
  let(:listing) { double 'listing', id: 1, sku: 'sku', variant_id: 12, local_id: 'local_id', local_item_id: 'local_item_id' }
  let(:listings) { [ id: 1, sku: 'sku', variant_id: 12, local_id: 'local_id', local_item_id: 'local_item_id' ] }
  let(:wh_sp) { double 'warehouse space', id: 1, warehouse_id: 76, item_variant_id: 12, quantity: 10 }
  let(:variant) { double 'variant', id: 1, item_id: 1, config: 'default', variant_listings: listings }
  let(:stock_allocation) { double 'stock allocation', id: 1, variant_association_id: 1, quantity: 3, start_at: start_at, end_at: end_at }
  let(:start_at) { DateTime.now }
  let(:end_at) { DateTime.now }

  describe '#perform' do
    it 'have to return correct quantity' do
      allow(listing).to receive(:consignment?).and_return(false)
      allow(listing).to receive(:active?).and_return(true)
      allow(listing).to receive(:present?).and_return(true)
      allow(listing).to receive(:variant_listing_stock_allocation).and_return(stock_allocation)
      allow(stock_allocation).to receive(:present?).and_return(true)
      allow(stock_allocation).to receive(:start_at).and_return(start_at)
      allow(start_at).to receive(:beginning_of_day).and_return(Time.now)
      allow(stock_allocation).to receive(:end_at).and_return(end_at)
      allow(end_at).to receive(:end_of_day).and_return(Time.now)
      allow(variant).to receive(:variant_listings).and_return(listings)
      allow(listings).to receive(:pluck).with(:id).and_return([1])
      allow(stock_allocation).to receive(:where).with('ADDTIME(end_at, "07:00") >= NOW()').and_return(stock_allocation)
      allow(VariantListingStockAllocation).to receive(:where).with(variant_association_id: [1]).and_return(stock_allocation)
      allow(stock_allocation).to receive(:blank?).and_return(false)
      allow(stock_allocation).to receive(:sum).with(:quantity).and_return(3)
      allow(stock_allocation).to receive(:each).and_return(3)
      expect(subject.perform).to be_eql(7)
    end
  end
end
