# frozen_string_literal: true

require 'json';
RSpec.describe ItemBuilder::Modes::Price::BukalapakService do
  subject { described_class.new(listing) }
  
  describe '#perform not sale' do
    let(:sale_start_at) { DateTime.now }
    let(:sale_end_at) { DateTime.now + 1 }
    let(:listing) { double 'listing', id: 1, sku: 'sku', price: 10000, sale_price: nil, sale_start_at: sale_start_at, sale_end_at: sale_end_at }
    let(:response) {
      {
        price: 10000,
        sale_price: nil,
        sale_start_at: sale_start_at,
        sale_end_at: sale_end_at
      }
    }
    it 'have to return correct hash' do
      expect(subject.perform).to be_eql(response)
    end
  end
end