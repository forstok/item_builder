# frozen_string_literal: true

require 'json';
RSpec.describe ItemBuilder::Modes::Price::SalePricePolicy do
  subject { described_class.new(listing: listing) }
  let(:sale_start_at) { DateTime.now }
  let(:sale_end_at) { DateTime.now + 1 }
  let(:listing) { double 'listing', id: 1, sku: 'sku', price: 10000, sale_price: 5000, sale_start_at: sale_start_at, sale_end_at: sale_end_at }

  describe '#sale_price' do
    it 'have to return correct sale_price' do
      expect(subject.sale_price).to be_eql(5000)
    end
  end

  describe '#on_sale?' do
    it 'have to return correct on_sale?' do
      expect(subject.on_sale?).to be_eql(true)
    end
  end
end