# frozen_string_literal: true

require 'json'
require 'date'
require 'byebug'
RSpec.describe ItemBuilder::Modes::Price::ShopifyService do
  subject { described_class.new(listing) }
  
  describe '#perform sale' do
    let(:sale_start_at) { DateTime.now }
    let(:sale_end_at) { DateTime.now + 1 }
    let(:listing) { double 'listing', id: 1, sku: 'sku', price: 10000, sale_price: 5000, sale_start_at: sale_start_at, sale_end_at: sale_end_at }
    let(:response) {
      {
        price: 10000,
        sale_price: 5000,
        sale_start_at: sale_start_at,
        sale_end_at: sale_end_at
      }
    }
    it 'have to return correct hash' do
      expect(subject.perform).to be_eql(response)
    end
  end

  describe '#perform not sale' do
    let(:sale_start_at) { DateTime.now - 2 }
    let(:sale_end_at) { DateTime.now - 1 }
    let(:listing) { double 'listing', id: 1, sku: 'sku', price: 10000, sale_price: 5000, sale_start_at: sale_start_at, sale_end_at: sale_end_at }
    let(:response) {
      {
        price: 10000,
        sale_price: 10000,
        sale_start_at: sale_start_at,
        sale_end_at: sale_end_at
      }
    }
    it 'have to return correct hash' do
      expect(subject.perform).to be_eql(response)
    end
  end
end
