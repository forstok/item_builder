# frozen_string_literal: true

class Variant;end
class WarehouseSpace;end
class VariantListingStockAllocation;end
require 'date'
require 'byebug';
RSpec.describe ItemBuilder::Modes::QuantityService do
  subject { described_class.new(params) }
  let(:params){
    { listing: listing }
  }
  let(:listing) { double 'listing', id: 1, sku: 'sku', local_id: 'local_id', variant_id: 12, local_item_id: 'local_item_id', price: 1230, sale_price: 123, sale_start_at: sale_start_at, sale_end_at: sale_end_at, channel_id: 15 }
  let(:listings) { [ id: 1, sku: 'sku', variant_id: 12, local_id: 'local_id', local_item_id: 'local_item_id' ] }
  let(:warehouse) { double 'warehouse', id: 1, warehouse_id: 1, item_variant_id: 12, quantity: 0 }
  let(:variant) { double 'variant', id: 12, item_id: 1, config: 'default' }
  let(:stock_allocation) { double 'stock allocation', id: 1, variant_association_id: 1, quantity: 3, start_at: start_at, end_at: end_at }
  let(:sale_start_at) { Time.now.to_s }
  let(:sale_end_at) { Time.now.to_s }
  let(:start_at) { DateTime.now }
  let(:end_at) { DateTime.now }

  before do
    allow(WarehouseSpace).to receive(:find_by).with(item_variant_id: 12).and_return(warehouse)
    allow(Variant).to receive(:find).with(listing.variant_id).and_return(variant)
    allow(listing).to receive(:consignment?).and_return(false)
    allow(listing).to receive(:active?).and_return(true)
    allow(listing).to receive(:present?).and_return(true)
    allow(listing).to receive(:variant_listing_stock_allocation).and_return(stock_allocation)
    allow(stock_allocation).to receive(:present?).and_return(true)
    allow(stock_allocation).to receive(:start_at).and_return(start_at)
    allow(start_at).to receive(:beginning_of_day).and_return(Time.now)
    allow(stock_allocation).to receive(:end_at).and_return(end_at)
    allow(end_at).to receive(:end_of_day).and_return(Time.now)
    allow(variant).to receive(:variant_listings).and_return(listings)
    allow(listings).to receive(:pluck).with(:id).and_return([1])
    allow(stock_allocation).to receive(:where).with('ADDTIME(end_at, "07:00") >= NOW()').and_return(stock_allocation)
    allow(VariantListingStockAllocation).to receive(:where).with(variant_association_id: [1]).and_return(stock_allocation)
    allow(stock_allocation).to receive(:blank?).and_return(false)
    allow(stock_allocation).to receive(:sum).with(:quantity).and_return(3)
    allow(stock_allocation).to receive(:each).and_return(3)
  end

  describe '#perform' do
    it 'have to return correct hash' do
      expect(subject.perform).to be_eql({:id=>1, :local_id=>"local_id", :local_item_id=>"local_item_id", :quantity=>-3, :sku=>"sku"})
    end
  end
  describe '#to_h' do
    it 'have to return correct hash' do
      expect(subject.to_h).to be_eql({:quantity=>-3})
    end
  end
end
