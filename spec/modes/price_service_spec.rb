RSpec.describe ItemBuilder::Modes::PriceService do
  subject { described_class.new(params) }
  let(:params){
    { listing: listing }
  }
  let(:listing) { double 'listing', id: 1, sku: 'sku', local_id: 'local_id', local_item_id: 'local_item_id', price: 1230, sale_price: 123, sale_start_at: sale_start_at, sale_end_at: sale_end_at, channel_id: 15 }
  let(:sale_start_at) { Time.now.to_s }
  let(:sale_end_at) { Time.now.to_s }

  describe '#perform' do
    it 'have to return correct hash' do
      expect(subject.perform).to be_eql({:id=>1, :local_id=>"local_id", local_item_id: 'local_item_id', :price=>1230, :sale_end_at=>sale_end_at, :sale_price=>123, :sale_start_at=>sale_start_at, :sku=>"sku"})
    end
  end
  describe '#to_h' do
    it 'have to return correct hash' do
      expect(subject.to_h).to be_eql({:price=>1230, :sale_end_at=>sale_end_at, :sale_price=>123, :sale_start_at=>sale_start_at})
    end
  end
end

