# frozen_string_literal: true

class RestClient;end
class RestClient::ExceptionWithResponse;end
RSpec.describe ItemBuilder::Modes::Quantity::LazadaService do
  subject { described_class.new(listing, available_quantity) }
  let(:listing) { double 'listing', id: 1, sku: 'sku', quantity: 1, variant_id: 12, local_id: 'local_id', local_item_id: 'local_item_id', profile_channel_association_id: 1234 }
  let(:available_quantity) { 6 }
  describe '#perform' do
    it 'have to return correct quantity' do
      allow(listing).to receive(:profile_channel_association_id).and_return(false)
      allow(RestClient).to receive(:get).and_return(3)
      expect(subject.perform).to be_eql(9)
    end
  end
  describe '#reserved_params' do
    it 'have to return correct params' do
      expect(subject.reserved_params).to be_eql('account_id=1234
          &item_variant_id=12')
    end
  end
  describe '#order_host' do
    it 'have to return correct order host' do
      expect(subject.order_host).to be_eql('orders.forstok.com/api/v2/item_line/reserved_stock')
    end
  end
end
