class WarehouseSpace;end
RSpec.describe ItemBuilder::Modes::ActiveService do
  subject { described_class.new(params) }
  let(:params){
    { listing: listing }
  }
  let(:listing) { double 'listing', id: 1, sku: 'sku', local_id: 'local_id', local_item_id: 'local_item_id', active: true}
  let(:active_variant) { double 'active variant listing' }
  let(:sale_start_at) { Time.now.to_s }
  let(:sale_end_at) { Time.now.to_s }

  before do
    allow(VariantListing).to receive(:where).with(local_item_id: 'local_item_id', active: 1).and_return(active_variant)
    allow(active_variant).to receive(:count).and_return(1)
  end
  describe '#perform' do
    it 'have to return correct hash' do
      expect(subject.perform).to be_eql({:id=>1, :active_variant=>1, :local_id=>"local_id", local_item_id: 'local_item_id', :active=>true, :sku=>"sku"})
    end
  end
  describe '#to_h' do
    it 'have to return correct hash' do
      expect(subject.to_h).to be_eql({:active=>true, :active_variant=>1})
    end
  end
end

